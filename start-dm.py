 #!/usr/bin/python3
import os
import sys

class startdm:

	def __init__(self):
		self.default = "lightdm"
		self.autologin = True
		self.user = "root"
		self.dmuser = "root"
		self.session= "xterm"
		self.isoleenv=False
		self.readconf()
 	
	def isfile(self,path):
 		return os.path.exsits(path) and os.path.isfile(path)
 	
	def readconf(self):
	 	f=open("/etc/start-dm/default.cfg","r").readlines()
	 	for line in f:
 			line=line.replace("\n","")
	 		if "default=" in line:
 				self.default=line.split("=")[1]
 			if "autologin=" in line:
 				self.autologin=(line.split("=")[1] == "True")
 			if "isole-env=" in line:
 				self.isoleenv=(line.split("=")[1] == "True")
 			if "dm-user=" in line:
 				self.dmuser=line.split("=")[1]
 			if "autologin-user=" in line:
 				self.user=line.split("=")[1]
 			if "autologin-session=" in line:
 				self.session=line.split("=")[1]
 	
	def start(self):
		if self.isoleenv:
			os.environ.clear()
		os.environ["XDG_SESSION_TYPE"]="x11"
		os.environ["XDG_RUNTIME_DIR"]="/tmp/runtime-dm"
		os.system("rm -rf /tmp/*")
		if(sys.argv[len(sys.argv)-1]=="stop"):
			os.system("killall "+self.default)
			os.system("killall "+self.session)
			exit(0)
		if self.autologin:
			os.system("exec su \""+self.user+"\" -c \"cd ~ ; xinit "+self.session+" &\"")
		else:
			os.system("exec su "+self.dmuser+" -c \""+self.default+" &\"")
		exit(0)
 	
startdm().start()
 	
 	
