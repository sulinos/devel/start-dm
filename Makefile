install:
	mkdir -p $(DESTDIR)/etc/init.d || true
	mkdir -p $(DESTDIR)/usr/libexec || true
	mkdir -p $(DESTDIR)/etc/start-dm || true
	install dm-init.sh $(DESTDIR)/etc/init.d/dm || true
	install start-dm.py $(DESTDIR)/usr/libexec/start-dm || true
	install default.cfg $(DESTDIR)/etc/start-dm/default.cfg || true
